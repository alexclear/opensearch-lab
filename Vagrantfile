# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.hostmanager.enabled = true
  config.hostmanager.manage_host = false
  config.hostmanager.manage_guest = true
  config.hostmanager.ignore_private_ip = false
  config.hostmanager.include_offline = true

  config.vm.define "es1" do |es1|
    es1.vm.box = "generic/ubuntu1804"
    es1.vm.hostname = "es1"

    es1.vm.provider :virtualbox do |v, override|
      v.gui = false
      v.customize ["modifyvm", :id, "--cpus", 4]
      v.customize ["modifyvm", :id, "--memory", 8192]
      v.customize ["modifyvm", :id, "--cableconnected1", "on"]
      v.customize ["modifyvm", :id, "--cableconnected2", "on"]
    end

    es1.vm.provider :libvirt do |v, override|
      v.cpu_mode = 'custom'
      v.cpu_model = 'kvm64'
      v.cpus = 4
      v.memory = 8192
    end

    es1.vm.provider :hyperv do |v, override|
      es1.vm.network "public_network", bridge: "Default Switch"
      v.cpus = 4
      v.memory = 8192
      v.maxmemory = 8192
    end

    es1.vm.provision "shell", inline: "apt-get update && apt-get install -y python python3-pip software-properties-common && add-apt-repository -y ppa:deadsnakes/ppa"
    es1.vm.provision "shell", inline: "apt-get install -y python3.7"
    es1.vm.provision "shell", inline: "update-alternatives --install /usr/bin/python python /usr/bin/python3.7 10"
    es1.vm.provision "shell", inline: "/usr/bin/apt-mark manual python3"
    es1.vm.provision "shell", inline: "sed -i -e '/127\.0\.2/d' /etc/hosts"
    es1.vm.provision "shell", inline: "sysctl net.ipv6.conf.all.disable_ipv6=1"
    es1.vm.provision "shell", inline: "sysctl net.ipv6.conf.default.disable_ipv6=1"
  end

  config.vm.define "es2" do |es2|
    es2.vm.box = "generic/ubuntu1804"
    es2.vm.hostname = "es2"

    es2.vm.provider :virtualbox do |v, override|
      v.gui = false
      v.customize ["modifyvm", :id, "--cpus", 4]
      v.customize ["modifyvm", :id, "--memory", 8192]
      v.customize ["modifyvm", :id, "--cableconnected1", "on"]
      v.customize ["modifyvm", :id, "--cableconnected2", "on"]
    end

    es2.vm.provider :libvirt do |v, override|
      v.cpu_mode = 'custom'
      v.cpu_model = 'kvm64'
      v.cpus = 4
      v.memory = 8192
    end

    es2.vm.provider :hyperv do |v, override|
      es2.vm.network "public_network", bridge: "Default Switch"
      v.cpus = 4
      v.memory = 8192
      v.maxmemory = 8192
    end

    es2.vm.provision "shell", inline: "apt-get update && apt-get install -y python python3-pip software-properties-common && add-apt-repository -y ppa:deadsnakes/ppa"
    es2.vm.provision "shell", inline: "apt-get install -y python3.7"
    es2.vm.provision "shell", inline: "update-alternatives --install /usr/bin/python python /usr/bin/python3.7 10"
    es2.vm.provision "shell", inline: "/usr/bin/apt-mark manual python3"
    es2.vm.provision "shell", inline: "sed -i -e '/127\.0\.2/d' /etc/hosts"
    es2.vm.provision "shell", inline: "sysctl net.ipv6.conf.all.disable_ipv6=1"
    es2.vm.provision "shell", inline: "sysctl net.ipv6.conf.default.disable_ipv6=1"
  end

  config.vm.define "es3" do |es3|
    es3.vm.box = "generic/ubuntu1804"
    es3.vm.hostname = "es3"

    es3.vm.provider :virtualbox do |v, override|
      v.gui = false
      v.customize ["modifyvm", :id, "--cpus", 4]
      v.customize ["modifyvm", :id, "--memory", 8192]
      v.customize ["modifyvm", :id, "--cableconnected1", "on"]
      v.customize ["modifyvm", :id, "--cableconnected2", "on"]
    end

    es3.vm.provider :libvirt do |v, override|
      v.cpu_mode = 'custom'
      v.cpu_model = 'kvm64'
      v.cpus = 4
      v.memory = 8192
    end

    es3.vm.provider :hyperv do |v, override|
      es3.vm.network "public_network", bridge: "Default Switch"
      v.cpus = 4
      v.memory = 8192
      v.maxmemory = 8192
    end

    es3.vm.provision "shell", inline: "apt-get update && apt-get install -y python python3-pip software-properties-common && add-apt-repository -y ppa:deadsnakes/ppa"
    es3.vm.provision "shell", inline: "apt-get install -y python3.7"
    es3.vm.provision "shell", inline: "update-alternatives --install /usr/bin/python python /usr/bin/python3.7 10"
    es3.vm.provision "shell", inline: "/usr/bin/apt-mark manual python3"
    es3.vm.provision "shell", inline: "sed -i -e '/127\.0\.2/d' /etc/hosts"
    es3.vm.provision "shell", inline: "sysctl net.ipv6.conf.all.disable_ipv6=1"
    es3.vm.provision "shell", inline: "sysctl net.ipv6.conf.default.disable_ipv6=1"
  end

  config.vm.define "es4" do |es4|
    es4.vm.box = "generic/ubuntu1804"
    es4.vm.hostname = "es4"

    es4.vm.provider :virtualbox do |v, override|
      v.gui = false
      v.customize ["modifyvm", :id, "--cpus", 4]
      v.customize ["modifyvm", :id, "--memory", 8192]
      v.customize ["modifyvm", :id, "--cableconnected1", "on"]
      v.customize ["modifyvm", :id, "--cableconnected2", "on"]
    end

    es4.vm.provider :libvirt do |v, override|
      v.cpu_mode = 'custom'
      v.cpu_model = 'kvm64'
      v.cpus = 4
      v.memory = 8192
    end

    es4.vm.provider :hyperv do |v, override|
      es4.vm.network "public_network", bridge: "Default Switch"
      v.cpus = 4
      v.memory = 8192
      v.maxmemory = 8192
    end

    es4.vm.provision "shell", inline: "apt-get update && apt-get install -y python python3-pip software-properties-common && add-apt-repository -y ppa:deadsnakes/ppa"
    es4.vm.provision "shell", inline: "apt-get install -y python3.7"
    es4.vm.provision "shell", inline: "update-alternatives --install /usr/bin/python python /usr/bin/python3.7 10"
    es4.vm.provision "shell", inline: "/usr/bin/apt-mark manual python3"
    es4.vm.provision "shell", inline: "sed -i -e '/127\.0\.2/d' /etc/hosts"
    es4.vm.provision "shell", inline: "sysctl net.ipv6.conf.all.disable_ipv6=1"
    es4.vm.provision "shell", inline: "sysctl net.ipv6.conf.default.disable_ipv6=1"
  end

  config.vm.define "es5" do |es5|
    es5.vm.box = "generic/ubuntu1804"
    es5.vm.hostname = "es5"

    es5.vm.provider :virtualbox do |v, override|
      override.vm.synced_folder ".", "/vagrant"
      v.gui = false
      v.customize ["modifyvm", :id, "--cpus", 4]
      v.customize ["modifyvm", :id, "--memory", 8192]
      v.customize ["modifyvm", :id, "--cableconnected1", "on"]
      v.customize ["modifyvm", :id, "--cableconnected2", "on"]
    end

    es5.vm.provider :libvirt do |v, override|
      v.cpu_mode = 'custom'
      v.cpu_model = 'kvm64'
      v.cpus = 4
      v.memory = 8192
    end

    es5.vm.provider :hyperv do |v, override|
      es5.vm.network "public_network", bridge: "Default Switch"
      override.vm.synced_folder ".", "/vagrant", type: "smb", smb_username: ENV["SMB_USERNAME"], smb_password: ENV["SMB_PASSWORD"]
      v.cpus = 4
      v.memory = 8192
      v.maxmemory = 8192
    end

    es5.vm.provision "shell", inline: "apt-get install -y python python3-pip software-properties-common && add-apt-repository -y ppa:deadsnakes/ppa"
    es5.vm.provision "shell", inline: "apt-get install -y python3.7"
    #es5.vm.provision "shell", inline: "update-alternatives --remove-all python"
    es5.vm.provision "shell", inline: "update-alternatives --install /usr/bin/python python /usr/bin/python3.7 10"
    es5.vm.provision "shell", inline: "/usr/bin/apt-mark manual python3"
    es5.vm.provision "shell", inline: "sed -i -e '/127\.0\.2/d' /etc/hosts"
    es5.vm.provision "shell", inline: "sysctl net.ipv6.conf.all.disable_ipv6=1"
    es5.vm.provision "shell", inline: "sysctl net.ipv6.conf.default.disable_ipv6=1"
    es5.vm.provision "shell", inline: "cp /vagrant/.vagrant/machines/es1/hyperv/private_key /home/vagrant/private_key_es1 && cp /vagrant/.vagrant/machines/es2/hyperv/private_key /home/vagrant/private_key_es2 && cp /vagrant/.vagrant/machines/es3/hyperv/private_key /home/vagrant/private_key_es3 && cp /vagrant/.vagrant/machines/es4/hyperv/private_key /home/vagrant/private_key_es4 && cp /vagrant/.vagrant/machines/es5/hyperv/private_key /home/vagrant/private_key_es5 && chmod 600 /home/vagrant/private_key* && chown vagrant:vagrant /home/vagrant/private_key* && ls -alF /home/vagrant/private_key*"
    es5.vm.provision "ansible_local" do |ansible|
      ansible.install_mode = "pip"
      ansible.playbook = "ansible/site.yml"
      ansible.config_file = "ansible/ansible.cfg"
      ansible.inventory_path = "ansible/inventory"
      ansible.become = true
      ansible.galaxy_role_file = "ansible/roles/requirements.yml"
      ansible.galaxy_roles_path = "/etc/ansible/roles"
      ansible.galaxy_command = "sudo ansible-galaxy collection install ansible.posix && sudo ansible-galaxy install --role-file=%{role_file} --roles-path=%{roles_path} --force"
      ansible.limit = "all"
    end
  end
end
